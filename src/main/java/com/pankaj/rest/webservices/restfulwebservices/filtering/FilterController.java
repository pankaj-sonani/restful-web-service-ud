package com.pankaj.rest.webservices.restfulwebservices.filtering;

import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class FilterController {

    @GetMapping(path = "/filter")
    public StaticFiltering getAllFileds(){
        return new StaticFiltering("field1", "field2","field3");
    }

    @GetMapping(path = "/filter-list")
    public MappingJacksonValue getListOfAllFileds(){

        List<DynamicFilter> list =  Arrays.asList(new DynamicFilter("field4", "field5", "field6"), new DynamicFilter("field1", "field2", "field3"));

        SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.filterOutAllExcept("fields4");

        FilterProvider filterProvider = new SimpleFilterProvider().addFilter("SomeBeanFilter", filter );

        MappingJacksonValue mappingJacksonValue = new MappingJacksonValue(list);

        mappingJacksonValue.setFilters(filterProvider);

        return mappingJacksonValue;
    }
}
