package com.pankaj.rest.webservices.restfulwebservices.filtering;

import com.fasterxml.jackson.annotation.JsonFilter;

@JsonFilter("SomeBeanFilter")
public class DynamicFilter {

    private String fields4;
    private String fields5;
    private String fields6;

    public DynamicFilter(String fields4, String fields5, String fields6) {
        this.fields4 = fields4;
        this.fields5 = fields5;
        this.fields6 = fields6;
    }

    public String getFields4() {
        return fields4;
    }

    public void setFields4(String fields4) {
        this.fields4 = fields4;
    }

    public String getFields5() {
        return fields5;
    }

    public void setFields5(String fields5) {
        this.fields5 = fields5;
    }

    public String getFields6() {
        return fields6;
    }

    public void setFields6(String fields6) {
        this.fields6 = fields6;
    }
}
