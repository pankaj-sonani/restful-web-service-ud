package com.pankaj.rest.webservices.restfulwebservices.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString @Setter @Getter
public class HelloWorldBean {

    private String helloWorld;

    public HelloWorldBean(String message) {
        this.helloWorld = message;
    }
}
