package com.pankaj.rest.webservices.restfulwebservices.controllers;

import com.pankaj.rest.webservices.restfulwebservices.models.HelloWorldBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorld {

    @Autowired
    private MessageSource messageSource;

    @GetMapping(value ="/hello")
    public String helloWorld(){
        return "Hello world from Controller";
    }

    @GetMapping(path = "/hellobean")
    public HelloWorldBean helloWorldBean (){
        return new HelloWorldBean ("Hello World from Bean");
    }

    @GetMapping(path = "/hellopathparam/{name}")
    public HelloWorldBean helloWorldPathParam(@PathVariable String name){
        return new HelloWorldBean(String.format("Hello World %s ", name));
    }

    //pass local with header arguments
    @GetMapping(path = "/i18n")
    public String messageI18N(){
        String message = messageSource.getMessage("good.morning.message", null, LocaleContextHolder.getLocale());
        return message;
    }
}
